<?php

/**
 * @file
 * uw_site_feds.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_site_feds_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
