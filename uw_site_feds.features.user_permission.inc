<?php

/**
 * @file
 * uw_site_feds.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_site_feds_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_writers content'.
  $permissions['create feds_writers content'] = array(
    'name' => 'create feds_writers content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_writers content'.
  $permissions['delete any feds_writers content'] = array(
    'name' => 'delete any feds_writers content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_writers content'.
  $permissions['delete own feds_writers content'] = array(
    'name' => 'delete own feds_writers content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_writers content'.
  $permissions['edit any feds_writers content'] = array(
    'name' => 'edit any feds_writers content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_writers content'.
  $permissions['edit own feds_writers content'] = array(
    'name' => 'edit own feds_writers content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_promo_banner revision log entry'.
  $permissions['enter feds_promo_banner revision log entry'] = array(
    'name' => 'enter feds_promo_banner revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
